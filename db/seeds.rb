# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(username:  "Example User",
             email: "test@test.com",
             profile: "こんにちは。"
             password:              "foobar",
             password_confirmation: "foobar")

50.times do |n|
  name  = Faker::BossaNova.artist
  email = "example-#{n+1}@test.com"
  profile = Faker::Beer.brand
  password = "password"
  User.create!(username:  name,
               email: email,
               profile: profile,
               password: password
               )
end

users = User.order(:created_at).take(6)
  50.times do
    artistname = "Marvin Gaye"
    albumtitle = "What's Going On"
    content = "💓"

    artworkimg = "https://is3-ssl.mzstatic.com/image/thumb/Music18/v4/a5/46/ba/a546ba2d-f2ba-54fd-2bba-4a9804c47bb9/source/200x200bb.jpg"
    users.each { |user| user.microposts.create!(content: content,
    artworkimg: artworkimg, artistname: artistname, albumtitle: albumtitle)}
  end
