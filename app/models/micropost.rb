class Micropost < ApplicationRecord
  belongs_to :user ,dependent: :destroy
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :content, presence: true
  # validates :artistname, presence: true
  # validates :albumtitle, presence:true
end
