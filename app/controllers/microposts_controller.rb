require 'net/https'
require 'uri'
require 'json'
class MicropostsController < ApplicationController
  before_action :authenticate_user!, only:[:create, :destroy]

  def create
      # hash形式でパラメタ文字列を指定し、URL形式にエンコード
      artistterm = micropost_params[:artistname]
      albumterm = micropost_params[:albumtitle]
      params = URI.encode_www_form({ term: "#{artistterm} #{albumterm}" ,country: 'jp' ,
            media: 'music' , entity: 'album' ,lang: 'ja_jp', limit: '10'})
      # URIを解析し、hostやportをバラバラに取得できるようにする
      uri = URI.parse("https://itunes.apple.com/search?#{params}")
      # リクエストパラメタを、インスタンス変数に格納
      @query = uri.query

      # 新しくHTTPセッションを開始し、結果をresponseへ格納
        https = Net::HTTP.new(uri.host, uri.port)
        https.use_ssl = true
        https.verify_mode = OpenSSL::SSL::VERIFY_NONE
        response = https.start do |http|
          # 接続時に待つ最大秒数を設定
          http.open_timeout = 5
          # 読み込み一回でブロックして良い最大秒数を設定
          http.read_timeout = 10
          # ここでWebAPIを叩いている
          # Net::HTTPResponseのインスタンスが返ってくる
          http.get(uri.request_uri)
        end

      # 例外処理の開始
      begin
        # responseの値に応じて処理を分ける
        case response
        # 成功した場合
        when Net::HTTPSuccess
          # responseのbody要素をJSON形式で解釈し、hashに変換
          @result = JSON.parse(response.body)
          # 表示用の変数に結果を格納
          @artistname = @result["results"][0]["artistName"]
          @trackname = @result["results"][0]["trackName"]
          @artworkUrl100 = @result["results"][0]["artworkUrl100"]
          #画像のリサイズ
          @resizeimg = @artworkUrl100.gsub(/100x100/, '200x200')
        # 別のURLに飛ばされた場合
        when Net::HTTPRedirection
          @message = "Redirection: code=#{response.code} message=#{response.message}"
        # その他エラー
        else
          @message = "HTTP ERROR: code=#{response.code} message=#{response.message}"
        end
      # エラー時処理
      rescue IOError => e
        @message = "e.message"
      rescue TimeoutError => e
        @message = "e.message"
      rescue JSON::ParserError => e
        @message = "e.message"
      rescue => e
        @message = "e.message"
      end
      #投稿データの保存
    @micropost = current_user.microposts.build(artistname: @artistname, albumtitle: @trackname,
                artworkimg: @resizeimg,content: micropost_params[:content])
    if @micropost.save
      redirect_to index_path, notice: "投稿完了"
    else
      flash.now[:alert] = "投稿できませんでした"
      render 'pages/index'
    end
  end

  def destroy
  end

  def show
    @micropost = Micropost.find(params[:id])
  end

  private
    def micropost_params
      params.require(:micropost).permit(:content,:artistname,:albumtitle)
    end
end
