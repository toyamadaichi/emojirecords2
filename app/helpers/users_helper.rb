module UsersHelper

  def userimage(user)
    userimageurl = '#{user.image}'
    image_tag(userimageurl)
  end
end
